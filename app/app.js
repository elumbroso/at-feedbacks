/**
 * @ngdoc overview
 * @name index
 * @description
 * Une super app AngularJS avec des gens sympa qui ont des passions étranges.
 */
var gyf = angular.module('gyf', [
        'ngRoute',
        'firebase'
    ])
    .config( [
      '$compileProvider',
      function( $compileProvider )
      {
          $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|data|file|mailto):/);
      }
    ])
    .config(['$routeProvider',
        function ($routeProvider) {

            $routeProvider.
            when('/home', {
                templateUrl: 'views/home/home.html',
                controller: homeCtrl
            }).
            when('/profile', {
                templateUrl: 'views/profile/profile.html',
                controller: profileCtrl
            }).
            when('/feedbacks', {
                templateUrl: 'views/feedbacks/feedbacks.html',
                controller: feedbacksCtrl
            })
            otherwise({
                redirectTo: '/feedbacks'
            });
        }
    ]);
