/**
 * @ngdoc service
 * @name demoApp.service:skelService
 * @scope
 * @description
 * Squelette de service
 */
 angular.module('gyf')
    .factory('auth', function ($firebaseAuth,dbService) {

      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          return user;
        } else {
          // No user is signed in.
        }
      });

    })
