
/**
 * @ngdoc controller
 * @name demoApp.controller:homeCtrl
 * @scope
 * @requires $scope
  * @description
 * Contrôleur pour la vue Home
 */
function feedbacksCtrl($scope,$firebaseArray,$firebaseObject) {
  var database = firebase.database();
  var databaseFeedbacks = firebase.database().ref('/feedbacks');

  $scope.feedbacks = [];
  $scope.loadingFeedback = true;
  $scope.noFeedbacks = false;
  $scope.hasFeedbackSelected = false;

  $scope.currentUser = '';

  var feedbacksNotArchive = databaseFeedbacks.orderByChild('isArchive').equalTo(false);
  $scope.feedbacks = $firebaseArray(feedbacksNotArchive);

  $scope.feedbacks.$loaded().then(function() {
    $scope.loadingFeedback = false;
    if($scope.feedbacks.length == 0) {
      $scope.noFeedbacks = true;
    }
  });

  $scope.selectFeedback = function(id) {
    $scope.hasFeedbackSelected = true;

    $('.gyf-feedback').removeClass('is-active');
    $('[data-feedback-id="'+id+'"]').addClass('is-active');

    var query = databaseFeedbacks.orderByChild('eid').equalTo(id);
    $scope.selectedFeedback = $firebaseArray(query);
  }

  $scope.updateStatus = function(id,st) {
    databaseFeedbacks.orderByChild('eid').equalTo(id).once('value').then(function(snap) {
      snap.forEach(function(s){
        var key = s.key;

        databaseFeedbacks.child(s.key).update({
          status : st
        })

        return true;
      })
    })
  }

  $scope.updateArchive = function(id,archive) {
    databaseFeedbacks.orderByChild('eid').equalTo(id).once('value').then(function(snap) {
      snap.forEach(function(s){
        var key = s.key;

        databaseFeedbacks.child(s.key).update({
          isArchive : archive
        })
        $scope.selectedFeedback = '';
        $scope.hasFeedbackSelected = false;
        return true;
      })
    })
  }
}
