/**
 * Created by yancorneille on 10/11/2015.
 */
/**
 * @ngdoc controller
 * @name demoApp.controller:homeCtrl
 * @scope
 * @requires $scope
  * @description
 * Contrôleur pour la vue Home
 */
function homeCtrl($scope,$location,auth) {

  var databaseUsers = firebase.database().ref('/users');
  console.log(auth);
  $scope.user = {
    email : '',
    password : ''
  };

  $scope.signUp = function() {

    var email = $scope.user.email;
    var password = $scope.user.password;
    console.log(email,password);
    firebase.auth().createUserWithEmailAndPassword(email, password).then(function(result){
      console.log(result);

    }, function(error) {
      console.log(error);
    })

  }

}
