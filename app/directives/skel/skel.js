/**
 * Created by yancorneille on 10/11/2015.
 */
/**
 * @ngdoc directive
 * @name demoApp.directive:skel
 * @restrict E
 * @description
 * Squelette de directive
 * @scope
 */
demoApp.directive('people', function () {
    return {

        restrict: 'E',
        templateUrl: 'directives/skel/skel.html',
        scope: {
            person: '=',
            selected: '='

        },

        link: function (scope, element, attrs, ngModel) {

        }
    }
});