/**
 * Created by yancorneille on 10/11/2015.
 */
module.exports = function (grunt) {

    var serveStatic = require('serve-static');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


        sass: {
            options: {
                sourceMap: false
            },
            dist: {
                files: {
                    'dist/css/main.css': 'app/scss/main.scss'
                }
            },
            dev:{
                files : {
                    'app/css/main.css' : 'app/scss/main.scss'
                }
            }

        },

        watch: {
            files: ['app/scss/**/*'],
            tasks: ['sass'],
        },

        connect: {
            app: {
                options: {
                    port       : 9000,
                    base       : 'app/',
                    open       : true,
                    hostname   : '127.0.0.1',
                    middleware: function(connect) {
                        return [
                            serveStatic('.tmp'),
                            connect().use('/bower_components', serveStatic('./bower_components')),
                            serveStatic('./app')
                        ];
                    }
                }
            }
        },

        clean: {
            all: ['dist', '.tmp','docs'],
            dist: ['.tmp']
        },
        copy: {
            dist: {
                files: [
                    {
                        src: ['app/*.html', 'app/views/**/*.html', 'app/directives/**/*.html', 'app/*.ico'],
                        dest: 'dist/'
                    },
                    {
                        src: ['app/**/*.png'],
                        dest: 'dist/'
                    }

                ]
            }
        },

        useminPrepare: {
            html: 'app/index.html',
            options: {
                root: 'app',
                dest: 'dist/app'
            }
        },
        ngAnnotate: {
            options: {
                // Task-specific options go here.
            },
            angular: {
                // Target-specific file lists and/or options go here.
                files: {'.tmp/concat/min.js': [".tmp/concat/min.js"]}
            }
        },
        usemin: {
            html: ['dist/app/index.html'],
            options: {
                assetsDirs: ['dist/app']
            }
        },

        filerev: {
            dist: {src: ['dist/**/*.js', 'dist/**/*.css']}

        },
        uglify: {
            options: {
                beautify: false,
                mangle: true
            }
        },

        ngdocs: {
            options: {
                dest: 'docs',
                html5Mode: false,
                scripts: [
                    'app/bower_components/angular/angular.min.js'
                ]
            },
            api: {
                src: ['app/**/*.js', '!app/bower_components/**/*.js'],
                title: 'Docs'
            }
        },
        protractor: {
            options: {
                configFile: "node_modules/protractor/example/conf.js", // Default config file
                keepAlive: true, // If false, the grunt process stops when the test fails.
                noColor: false, // If true, protractor will not use colors in its output.
                args: {
                    // Arguments passed to the command
                }
            },
            your_target: {   // Grunt requires at least one target to run so you can simply put 'all: {}' here too.
                options: {
                    configFile: "tests/conf.js", // Target-specific config file
                    args: {} // Target-specific arguments
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-filerev');
    grunt.loadNpmTasks('grunt-ngdocs');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-protractor-runner');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');


    grunt.registerTask('build', ['clean:all', 'copy:dist', 'useminPrepare', 'concat:generated', 'ngAnnotate', 'cssmin:generated', 'uglify:generated', 'filerev', 'usemin', 'clean:dist']);
    grunt.registerTask('serve',['connect','watch']);

};
